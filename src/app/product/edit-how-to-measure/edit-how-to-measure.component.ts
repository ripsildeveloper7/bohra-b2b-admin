import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { ProductService } from '../product.service';
import { MatSnackBar } from '@angular/material';
import { AppSetting } from 'src/app/config/appSetting';
import { HowToMeasure } from '../how-to-measure/how-to-measure.model';

@Component({
  selector: 'app-edit-how-to-measure',
  templateUrl: './edit-how-to-measure.component.html',
  styleUrls: ['./edit-how-to-measure.component.css']
})
export class EditHowToMeasureComponent implements OnInit {
  howToMeasureUrl: any;
  id: string;
  holder: any;
  detailsForm: any;
  howToMeasureForm: HowToMeasure;

  constructor(private fb: FormBuilder, private router: Router, private productService: ProductService,
    private snackBar: MatSnackBar, private activatedRoute: ActivatedRoute) {
      this.howToMeasureUrl = AppSetting.howToMeasureImageUrl ;
this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
this.id = params.get('id');
});
}

  ngOnInit() {
    this.getSingleSizeGuide();
  }

  createForm() {
    // this.howToMeasureForm = this.fb.group({

    // })
    this.detailsForm = this.fb.group({
      detailHeading:[''],
      details: this.fb.array([])
    });
    this.addDetailForm();
  }
  addDetailForm() {
    const detail = this.fb.group({
      name: ['', Validators.required],
      detail: ['', Validators.required],
    });
    this.detailForm.push(detail);
  }
  get detailForm() {
    return this.detailsForm.get('details') as FormArray;
  }
  deleteDetail(i) {
    this.detailForm.removeAt(i);
  }
  getSingleSizeGuide() {
    this.productService.getSingleHowToMeasure(this.id).subscribe(data => {
      this.holder = data;
      console.log(this.holder);
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
  cancelUpdate() {
    this.router.navigate(['product/howtomeasure']);
  }
}
