import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { SalesService } from '../sales.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-shipping-fees',
  templateUrl: './shipping-fees.component.html',
  styleUrls: ['./shipping-fees.component.css']
})
export class ShippingFeesComponent implements OnInit {
  holder: any;
  showError :boolean;
  shippingFeeForm:FormGroup;
  create: {
    minimumPrice: number;
    fees: number;
  };
  modify: {
    status: boolean
  };
  constructor(private salesService: SalesService,private fb: FormBuilder) {
    this.getShippingFees();
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.shippingFeeForm = this.fb.group({
     
      minimumPrice: ['', Validators.required],
      fees: ['', Validators.required],
      status:[''],
      
    });
   
  }
  getShippingFees() {
    this.salesService.getShippingFees().subscribe(data => {
      this.holder = data[0];
      console.log(data,'fees');
    }, error => {
      console.log(error);
    });
  }
  checkValue(min, fees) {
    if (min === '' || fees === '' ) {
      this.showError = true;
    } else {
      this.showError = false;
    }
    this.addFees(min, fees);
    console.log(min, fees);
  }
  addFees(min, fee) {
    
    this.create = {
      minimumPrice: this.shippingFeeForm.controls.minimumPrice.value,
      fees: this.shippingFeeForm.controls.fees.value
    };
    
    this.salesService.addShippingFees(this.create).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
  changeStatus(element) {
    console.log(element.target.checked);
    this.modify = {
      status: element.target.checked
    };
    this.salesService.updateShippingFeesStatus(this.modify).subscribe(data => {
      this.holder = data;
    }, error => {
      console.log(error);
    });
  }
}
