import { Component, OnInit } from '@angular/core';
import {SalesService} from '../sales.service';
import {Order} from '../orders/order.model';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import {AppSetting} from '../../config/appSetting';
import {CustomerModel} from './customer.model';
import { TailoringDetailService } from '../view-tailoring-detail/tailoring-detail.service';
import { ReviewPopupComponent } from '../review-popup/review-popup.component';

@Component({
  selector: 'app-view-single-order',
  templateUrl: './view-single-order.component.html',
  styleUrls: ['./view-single-order.component.css']
})
export class ViewSingleOrderComponent implements OnInit {
  id;
  orderModel: any;
  orderForm: FormGroup;
  productModel: any;
  message;
  action;
  serviceUrl;
  customerId;
  customerDetail: CustomerModel;
  isCoupon = false;
  productImageUrl: string = AppSetting.productImageUrl;
  status = ['New', 'Processing', 'Out For Delivery', 'OnHold', 'Completed', 'Cancelled', 'Failed'];
  reviewModel: any;
  constructor(private router: Router, private salesService: SalesService, private route: ActivatedRoute,
              private snackBar: MatSnackBar, private fb: FormBuilder,
              private tailoringService: TailoringDetailService, public dialog: MatDialog) {
    this.id = this.route.snapshot.params.id;
  }
  openDialog(child): void {
    const dialogRef = this.dialog.open(ReviewPopupComponent, {
      data: child, width: '300px', hasBackdrop: true
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      /* this.animal = result; */
    });
    
  }


  ngOnInit() {
    this.createForm();
    this.viewOrderDetails();
  }
  createForm() {
    this.orderForm = this.fb.group({
      orderedDate: [''],
      statusType: [''],
      AWBNo: ['']
    });
  }
  viewOrderDetails() {
    this.salesService.getOrderDetails(this.id).subscribe(data => {
      this.orderModel = data;
      if (this.orderModel.coupon.length === 0) {
        this.isCoupon = false;
      } else {
        this.isCoupon = true;
      }
      this.customerId = data.customerId;
      this.getCustomerDetail( this.customerId);
      this.getReviewByCustomer(this.orderModel.orderId);
      console.log('single order details', this.orderModel);
    }, err => {
      console.log(err);
    });
  }
  getReviewByCustomer(id) {
    this.salesService.getReviewByOrder(id).subscribe(data => {
      this.reviewModel = data;
      this.checkReview();
    }, error => {
      console.log(error);
    });
  }
  checkReview() {
    for (const review of this.reviewModel) {
      for (const cart of this.orderModel.cart) {
        if (review.cartId === cart._id) {
          for (const product of this.orderModel.orderedProducts) {
            for (const child of product[0].child) {
              if (child.INTsku === cart.INTsku) {
                child.isReview = true;
                child.review = review.review;
                child.rating = review.rating;
              }
            }
          }
        }
      }
    }
    console.log(this.orderModel);
  }
  getCustomerDetail(id) {
    this.salesService.getSingleCustomer(id).subscribe(data => {
      this.customerDetail = data;
      console.log('customer details', data);
    });
  }
  updateStatus()  {
    this.message = 'Order Updated';
    this.orderModel = new Order();
    this.orderModel.orderStatus = this.orderForm.controls.statusType.value;
    this.salesService.updateStatus(this.id, this.orderModel).subscribe(data => {
      this.orderModel = data;
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
     /*  this.router.navigate(['orders/vieworders']); */
    }, err => {
      console.log(err);
    });
  }
  updateAWB() {
    this.message = 'Order Updated';
    this.orderModel = new Order();
    this.orderModel.orderStatus = this.orderForm.controls.statusType.value;
    this.orderModel.awbNo = this.orderForm.controls.AWBNo.value;
    this.salesService.updateAWBStatus(this.id, this.orderModel).subscribe(data => {
      this.orderModel = data;
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
     /*  this.router.navigate(['orders/vieworders']); */
    }, err => {
      console.log(err);
    });
  }

  poGenerate() {
    this.router.navigate(['sales/purchaseOrderView/', this.id]);
  }
  getMeasurment(value) {
    console.log(value);
    this.tailoringService.open(value);
   /*  this.salesService.getSelctedMeasurementByUser(serviceId).subscribe(data => {
      const holder = data;
      this.tailoringService.open(holder);
    }, error => {
      console.log(error);
    }); */
  }
}
