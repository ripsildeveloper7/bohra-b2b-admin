/* export const environment = {
  production: true,
  imageUploadServiceUrl: 'https://70mnmbuddl.execute-api.ap-south-1.amazonaws.com/qa/',
  mainCategoryBannerImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/maincategory/',
  measurementImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/measurement/',
  productServiceUrl: 'https://7hs6v5qhs4.execute-api.ap-south-1.amazonaws.com/qa/',
  productImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/product/',
  sizeGuideImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/size/',
  categoryImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/category/',
  cmsServiceUrl: 'https://065h6q6de3.execute-api.ap-south-1.amazonaws.com/qa/',
  categoryBannerImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/categorybanner/',
  commerceOrderServiceUrl: 'https://ddxia2qpii.execute-api.ap-south-1.amazonaws.com/qa/',
  customerSerivceUrl: 'https://7vkipfu9da.execute-api.ap-south-1.amazonaws.com/qa/',
  subCategoryImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/subcategory/',
  brandImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/brand/',
  vendorImageServiceUrl: 'https://ucchalfashion-images.s3.ap-south-1.amazonaws.com/images/',
  marketingServiceUrl: 'https://9p6levewy7.execute-api.ap-south-1.amazonaws.com/qa/',
  excelUrl: 'https://ucchalfashion-images.s3.ap-south-1.amazonaws.com/excel/',
  howToMeasureImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/howtomeasure/',
  appId: 'ucchal'
}; */

// qa-environment

export const environment = {
  production: true,
  imageUploadServiceUrl: 'https://x1zqq7o9fj.execute-api.ap-south-1.amazonaws.com/qa/',
  mainCategoryBannerImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/maincategory/',
  measurementImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/measurement/',
  productServiceUrl: 'https://3gr03xpvlc.execute-api.ap-south-1.amazonaws.com/qa/',
  productImageUrl:  'http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/product/',
  sizeGuideImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/size/',
  categoryImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/category/',
  cmsServiceUrl: 'https://ohof6aswlc.execute-api.ap-south-1.amazonaws.com/qa/',
  categoryBannerImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/categorybanner/',
  commerceOrderServiceUrl: 'https://qwv6tk7tc6.execute-api.ap-south-1.amazonaws.com/qa/',
  customerSerivceUrl: 'https://35fs3a7crd.execute-api.ap-south-1.amazonaws.com/qa/',
  subCategoryImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/subcategory/',
  brandImageUrl: 'http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/brand/',
  vendorImageServiceUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/vendor/',
  marketingServiceUrl: 'https://pgrvf8whti.execute-api.ap-south-1.amazonaws.com/qa/',
  excelUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/excel/',
  howToMeasureImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/howtomeasure/',
  appId: 'ucchal'
};
